﻿using Microsoft.EntityFrameworkCore;
using RestAPIDemo.Database.Configurations;
using RestAPIDemo.Database.Models;

namespace RestAPIDemo.Database
{
    public class WebPortalDBContext : DbContext
    {
        public DbSet<Item> Items {get; set;}
        public DbSet<Worker> Workers { get; set; }
        public DbSet<Priority> Priorities { get; set; }

        public WebPortalDBContext(DbContextOptions<WebPortalDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {            
            modelBuilder.ApplyConfiguration(new ItemConfiguration());
            modelBuilder.ApplyConfiguration(new PriorityConfiguration());
            modelBuilder.ApplyConfiguration(new WorkerConfiguration());
        }
    }
}
