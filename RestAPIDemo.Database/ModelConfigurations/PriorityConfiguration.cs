﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RestAPIDemo.Database.Models;

namespace RestAPIDemo.Database.Configurations
{
    public class PriorityConfiguration : IEntityTypeConfiguration<Priority>
    {
        public void Configure(EntityTypeBuilder<Priority> builder)
        {
            builder.HasKey(v => v.Id);
            builder.Property(v => v.PriorityName);
            builder.HasMany(v => v.Items);
        }
    }
}
