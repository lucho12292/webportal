﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RestAPIDemo.Database.Models;

namespace RestAPIDemo.Database.Configurations
{
    public class WorkerConfiguration : IEntityTypeConfiguration<Worker>
    {
        public void Configure(EntityTypeBuilder<Worker> builder)
        {
            builder.HasKey(v => v.Id);
            builder.Property(v => v.Name);
            builder.Property(v => v.LastName);
            builder.Property(v => v.Email);           
        }
    }
}
