﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace RestAPIDemo.Database.Repositories
{
    public interface IUnitOfWork: IDisposable
    {
        DbContext Context { get; }
        void Commit();
        Task<int> CommitAsync();
    }
}
