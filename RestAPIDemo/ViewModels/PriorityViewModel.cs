﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.API.ViewModels
{
    public class PriorityViewModel
    {
        public int Id { get; set; }
        public string PriorityName { get; set; }
    }
}
