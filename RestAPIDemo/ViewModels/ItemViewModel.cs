﻿using RestAPIDemo.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.API.ViewModels
{
    public class ItemViewModel
    {        
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int LevelId { get; set; }
    }
}
